﻿using CoolParking.BL.Models;
using CoolParking.BL.Services;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Text.RegularExpressions;

namespace WebApi.Controllers
{
    [Route("api/")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        static ParkingService parkingService;
        public ParkingController(ParkingService service)
        {
            parkingService = service;
        }

        // GET api/parking/balance
        [HttpGet("parking/balance")]
        public IActionResult GetBalance()
        {
            return Ok(JsonConvert.SerializeObject(parkingService.GetBalance()));
        }

        // GET api/parking/capacity
        [HttpGet("parking/capacity")]
        public IActionResult GetСapacity()
        {
            return Ok(JsonConvert.SerializeObject(parkingService.GetCapacity()));
        }

        // GET api/parking/freePlaces
        [HttpGet("parking/freePlaces")]
        public IActionResult GetFreePlaces()
        {
            return Ok(JsonConvert.SerializeObject(parkingService.GetFreePlaces()));
        }

        //GET api/vehicles
        [HttpGet("vehicles")]
        public IActionResult GetVehicles()
        {
            return Ok(JsonConvert.SerializeObject(parkingService.GetVehicles()));
        }

        // GET api/vehicles/id (id - vehicle id of format “AA-0001-AA”)
        [HttpGet("vehicles/{id}")]
        public IActionResult GetVehicle(string id)
        {
            if (Regex.IsMatch(id, @"^[A-Z]{2}-\d{4}-[A-Z]{2}$"))
            {
                foreach (var vehicle in parkingService.GetVehicles())
                {
                    if (id == vehicle.Id)
                    {
                        return Ok(JsonConvert.SerializeObject(vehicle));
                    }
                }
                return NotFound("Vehicle id not found. There is no vehicle with such ID in the parking.");
            }
            else
                return BadRequest("Invalid vehicle id.");
        }

        // GET api/transactions/last
        [HttpGet("transactions/last")]
        public IActionResult GetTransactionsLast()
        {
            return Ok(JsonConvert.SerializeObject(parkingService.GetLastParkingTransactions()));
        }

        // GET api/transactions/all (тільки транзакції з лог файлу)
        [HttpGet("transactions/all")]
        public IActionResult GetTransactionsAll()
        {
            try
            {
                return Ok(JsonConvert.SerializeObject(parkingService.ReadFromLog()));
            }
            catch (InvalidOperationException e)
            {
                return NotFound(e.Message);
            }
        }

        // POST api/vehicles
        [HttpPost("vehicles")]
        public IActionResult AddVehile([FromBody] object content)
        {
            try 
            {
                Vehicle _vehicle = JsonConvert.DeserializeObject<Vehicle>(content.ToString());
                if (_vehicle.Id == null || (int)_vehicle.VehicleType < 1 || (int)_vehicle.VehicleType > 4)
                    throw new InvalidOperationException("Invalid body");
                parkingService.AddVehicle(_vehicle);
                return Created("api/vehicles", _vehicle);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            catch (InvalidOperationException e)
            {
                return BadRequest(e.Message);
            }
        }

        // DELETE api/vehicles/id (id - vehicle id of format “AA-0001-AA”)
        [HttpDelete("vehicles/{id}")]
        public IActionResult RemuveVehile(string id)
        {
            try
            {
                if (!Regex.IsMatch(id, @"^[A-Z]{2}-\d{4}-[A-Z]{2}$")) throw new InvalidOperationException("Invalid ID");
                parkingService.RemoveVehicle(id);
                return NoContent();
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
            catch (InvalidOperationException e)
            {
                return BadRequest(e.Message);
            }
        }

        // PUT api/transactions/topUpVehicle
        [HttpPut("transactions/topUpVehicle")]
        public IActionResult TopUpVehicle([FromBody] object content)
        {
            try
            {
                DataTransferObject data = JsonConvert.DeserializeObject<DataTransferObject>(content.ToString());
                parkingService.TopUpVehicle(data.Id, data.Sum);
                Vehicle _vehicle = null;
                foreach (var vehicle in parkingService.GetVehicles())
                {
                    if (data.Id == vehicle.Id)
                    {
                        _vehicle = vehicle; 
                    }
                }
                return Ok(JsonConvert.SerializeObject(_vehicle));
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            catch (InvalidOperationException e)
            {
                return BadRequest(e.Message);
            }
        }
        class DataTransferObject
        {
            public string Id { get; set; }
            public decimal Sum { get; set; }
            public DataTransferObject(string id, decimal sum) { Id = id; Sum = sum; }
        }
    }
}
