﻿using System;
using System.Collections;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json;

namespace UserInterface
{
    class Program
    {
        const string localhost = "https://localhost:44387/";
        static HttpClient client = new HttpClient();
        #region Menu
        
        private static System.Collections.Generic.List<string> menuMain = new System.Collections.Generic.List<string>() 
        {
            "Parking info...",
            "Add vehicle",
            "Remove vehicle",
            "Top up vehicle\n\n",
            "Exit"
        };
        private static System.Collections.Generic.List<string> menuParking = new System.Collections.Generic.List<string>()
        {
            "Current balance","Number of free parking places",
            "Sum of profit for the current period",
            "List of vehicles in the parking",
            "Transactions info...\n\n",
            "Return"
        };
        private static System.Collections.Generic.List<string> menuTransactions = new System.Collections.Generic.List<string>()
        {
            "Last parking transactions",
            "Transactions history\n\n",
            "Return"
        };
        private static int index = -1;
        private static void DrawCoolParking()
        {
            Console.BackgroundColor = ConsoleColor.DarkMagenta;
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(new string(' ', 93));
            Console.WriteLine(new string(' ', 3) + "░█████╗░░█████╗░░█████╗░██╗░░     ██████╗░░█████╗░██████╗░██╗░░██╗██╗███╗░░██╗░██████╗░" + new string(' ', 3) + "\n" +
                              new string(' ', 3) + "██╔══██╗██╔══██╗██╔══██╗██║░░     ██╔══██╗██╔══██╗██╔══██╗██║░██╔╝██║████╗░██║██╔════╝░" + new string(' ', 3) + "\n" +
                              new string(' ', 3) + "██║░░╚═╝██║░░██║██║░░██║██║░░     ██████╔╝███████║██████╔╝█████═╝░██║██╔██╗██║██║░░██╗░" + new string(' ', 3) + "\n" +
                              new string(' ', 3) + "██║░░██╗██║░░██║██║░░██║██║░░     ██╔═══╝░██╔══██║██╔══██╗██╔═██╗░██║██║╚████║██║░░╚██╗" + new string(' ', 3) + "\n" +
                              new string(' ', 3) + "╚█████╔╝╚█████╔╝╚█████╔╝███████╗  ██║░░░░░██║░░██║██║░░██║██║░╚██╗██║██║░╚███║╚██████╔╝" + new string(' ', 3) + "\n" +
                              new string(' ', 3) + "░╚════╝░░╚════╝░░╚════╝░╚══════╝  ╚═╝░░░░░╚═╝░░╚═╝╚═╝░░╚═╝╚═╝░░╚═╝╚═╝╚═╝░░╚══╝░╚═════╝░" + new string(' ', 3));
            Console.WriteLine(new string(' ', 93));
            Console.ResetColor();
        }
        private static int DrawMenu(System.Collections.Generic.List<string> items)
        {
            for (int i = 0; i < items.Count; i++)
            {
                if (i == index)
                {
                    //Console.BackgroundColor = ConsoleColor.Gray;
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine(new string(' ', 6) + "->  " + items[i]);
                }
                else
                {
                    Console.WriteLine(new string(' ', 10) + items[i]);
                }
                Console.ResetColor();
            }
            ConsoleKeyInfo key = Console.ReadKey();
            switch (key.Key)
            {
                case ConsoleKey.DownArrow:
                    if (index == items.Count - 1) index = 0;
                    else index++;
                    break;
                case ConsoleKey.UpArrow:
                    if (index <= 0) index = items.Count - 1;
                    else index--;
                    break;
                case ConsoleKey.Enter:
                    return index + 1;
                default:
                    Console.Clear();
                    return 0;
            }
            Console.Clear();
            return 0;
        }
        private static void DrawMainMenu()
        {
            while (true)
            {
                DrawCoolParking();
                Console.SetCursorPosition(0, 13);
                int selectedMenuItem = DrawMenu(menuMain);
                switch (selectedMenuItem)
                {
                    case 1:
                        DrawParkingMenu();
                        break;
                    case 2:
                        Console.Clear();
                        AddVehicle();
                        Console.WriteLine("Pres any key...");
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 3:
                        Console.Clear();
                        RemoveVehicle();
                        Console.WriteLine("Pres any key...");
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 4:
                        Console.Clear();
                        TopUpBalance();
                        Console.WriteLine("Pres any key...");
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 5:
                        Environment.Exit(0);
                        break;
                    default:
                        break;
                }

            }
        }
        private static void DrawParkingMenu()
        {
            index = -1;
            Console.Clear();
            while (true)
            {
                DrawCoolParking();
                Console.SetCursorPosition(0, 13);
                int selectedMenuParking = DrawMenu(menuParking);
                switch (selectedMenuParking)
                {
                    case 1:
                        Console.Clear();
                        YellowPrint($"Curent balance of the parking:");
                        Console.WriteLine(GetBalance());
                        Console.WriteLine("Pres any key...");
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 2:
                        Console.Clear();
                        YellowPrint($"Number of free parking places:  free {GetFreePlaces()} with {GetCapacity()}");
                        Console.WriteLine("Pres any key...");
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 3:
                        Console.Clear();
                        YellowPrint($"Current income: {CurrentProfit()}");
                        Console.WriteLine("Pres any key...");
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 4:
                        Console.Clear();
                        YellowPrint("List of vehicles:\n");
                        PrintListOfVehicles();
                        Console.WriteLine("Pres any key...");
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 5:
                        DrawTransactionMeny();
                        break;
                    case 6:
                        Console.Clear();
                        index = -1;
                        DrawMainMenu();
                        break;
                    default:
                        break;
                }

            }
        }
        private static void DrawTransactionMeny()
        {
            index = -1;
            Console.Clear();
            while (true)
            {
                DrawCoolParking();
                Console.SetCursorPosition(0, 13);
                int selectedmenuTransactions = DrawMenu(menuTransactions);
                switch (selectedmenuTransactions)
                {
                    case 1:
                        Console.Clear();
                        YellowPrint("Parking Transactions for the current period: ");
                        PrintLastParkingTransactions();
                        Console.WriteLine("Pres any key...");
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 2:
                        Console.Clear();
                        YellowPrint("Transaction history:");
                        string response = GetReadLog(out bool IsOk);
                        if (IsOk)
                            Console.WriteLine(JsonConvert.DeserializeObject<string>(response));
                        else
                            RedPrint(response);
                        Console.WriteLine("Pres any key...");
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 3:
                        Console.Clear();
                        index = -1;
                        DrawParkingMenu();
                        break;
                    default:
                        break;
                }
            }
        }

        #endregion

        #region servises

        static decimal GetBalance()
        {
            return JsonConvert.DeserializeObject<decimal>(client.GetStringAsync(localhost+"api/parking/balance").Result);
        }
        static int GetFreePlaces()
        {
            return JsonConvert.DeserializeObject<int>(client.GetStringAsync(localhost + "api/parking/freePlaces").Result);
        }
        static int GetCapacity()
        {
            return JsonConvert.DeserializeObject<int>(client.GetStringAsync(localhost + "api/parking/capacity").Result);
        }
        static string GetReadLog(out bool IsOk)
        {
            var response = client.GetAsync(localhost + "api/transactions/all").Result;
            IsOk = response.IsSuccessStatusCode;
            return response.Content.ReadAsStringAsync().Result;
        }
        static ArrayList GetVehicles()
        {
            return JsonConvert.DeserializeObject<ArrayList>(client.GetStringAsync(localhost + "api/vehicles").Result);
        }
        static ArrayList GetLastParkingTransactions()
        {
            return JsonConvert.DeserializeObject<ArrayList>(client.GetStringAsync(localhost + "api/transactions/last").Result);

        }
        static decimal CurrentProfit()
        {
            decimal income = 0;
            foreach (dynamic transactionInfo in GetLastParkingTransactions())
            {
                income += (decimal)transactionInfo.Sum;
            }
            return income;
        }
        static void AddVehicle()
        {
            string id;
            int type;
            decimal balance;
            Console.Write("Enter vehicle id: ");
            id = Console.ReadLine();
            Console.WriteLine("\n  Vehicle Types:");
            PrintVehicleTypes();
            while (true)
            {
                Console.Write("Enter number of vehicle type: ");
                try
                {
                    type = Convert.ToInt32(Console.ReadLine());
                    if (type > 0 && type < 5)
                        break;
                    else RedPrint("Invalid number od vehicle type");
                }
                catch (FormatException)
                {
                    RedPrint("Inaccessible format.");
                }
            }
            while (true)
            {
                try
                {
                    Console.Write("Enter vehicle balance: ");
                    balance = Convert.ToDecimal(Console.ReadLine());
                    break;
                }
                catch (FormatException)
                {
                    RedPrint("Inaccessible format.");
                }
            }
            var veacle = new { Id = id, VehicleType = type, Balance = balance };
            var content = new StringContent(JsonConvert.SerializeObject(veacle), System.Text.Encoding.UTF8, "application/json");
            var response = client.PostAsync(localhost + "api/vehicles", content).Result;
            if (response.StatusCode == HttpStatusCode.Created)
            {
                GreenPrint("Successfully");
            }
            else
            {
                RedPrint(response.Content.ReadAsStringAsync().Result);
            }
        }
        static void RemoveVehicle()
        {
            string id;
            Console.Write("Enter vehicle id: ");
            id = Console.ReadLine();
            var response = client.DeleteAsync(localhost + $"api/vehicles/{id}").Result;
            if (response.StatusCode == HttpStatusCode.NoContent)
            {
                GreenPrint("Successfully");
            }
            else
            {
                RedPrint(response.Content.ReadAsStringAsync().Result);
            }
        }
        static void TopUpBalance()
        {

            string id;
            decimal sum;
            Console.Write("Enter vehicle id: ");
            id = Console.ReadLine();
            while (true)
            {
                try
                {
                    Console.Write("Enter sum: ");
                    sum = Convert.ToDecimal(Console.ReadLine());
                    break;
                }
                catch (FormatException)
                {
                    RedPrint("Inaccessible format.");
                }
            }
            var valuePair = new { Id = id, Sum = sum };
            var content = new StringContent(JsonConvert.SerializeObject(valuePair), System.Text.Encoding.UTF8, "application/json");
            var response = client.PutAsync(localhost + $"api/transactions/topUpVehicle", content).Result;
            if (response.StatusCode == HttpStatusCode.OK)
            {
                GreenPrint("Successfully");
            }
            else
            {
                RedPrint(response.Content.ReadAsStringAsync().Result);
            }
        }

        // Print for menu

        static void PrintVehicleTypes()
        {
            string vehicleTypes = "1. PassengerCar\n" +
                "2. Truck\n" +
                "3. Bus\n" +
                "4. Motorcycle\n";
            Console.WriteLine(vehicleTypes);
        }
        static void PrintListOfVehicles()
        {
            foreach (dynamic vehicle in GetVehicles())
                Console.WriteLine($"{vehicle.Id} {vehicle.VehicleType} {vehicle.Balance}");
            Console.WriteLine();
        }
        static void PrintLastParkingTransactions()
        {
            foreach (dynamic transactionInfo in GetLastParkingTransactions())
                Console.WriteLine($"{transactionInfo.Id} {transactionInfo.Sum} {transactionInfo.Time}");
            Console.WriteLine();
        }
        
        #endregion

        #region color print

        static void YellowPrint(string str)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(str);
            Console.ResetColor();
        }
        static void RedPrint(string str)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(str);
            Console.ResetColor();
        }
        static void GreenPrint(string str)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(str);
            Console.ResetColor();
        }

        #endregion
        private static void Main(string[] args)
        {
            Console.SetWindowSize(93, 30);
            Console.CursorVisible = false;
            while (true)
            {
                DrawMainMenu();
            }
        }
    }
}