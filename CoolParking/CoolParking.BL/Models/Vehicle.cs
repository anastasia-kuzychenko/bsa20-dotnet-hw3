﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.

//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

//			Транспортні засоби (Тр. засоби) повинні мати Тип (один з 4х: Легкова, Вантажна, Автобус, Мотоцикл), Баланс та ID формату 
//			ХХ-YYYY-XX (де X - будь-яка літера англійського алфавіту у верхньому регістрі, а Y - будь-яка цифра, наприклад DV-2345-KJ).
using Newtonsoft.Json;
using System;
using System.Text.RegularExpressions;
namespace CoolParking.BL.Models
{
    public class Vehicle
    {
		public string Id { get; }
		public VehicleType VehicleType { get; }
		public decimal Balance { get; internal set; }
		[JsonConstructor]
		public Vehicle(string id, int vehicleType, decimal balance) : this(id, (VehicleType)vehicleType, balance) { }
		public Vehicle (string id, VehicleType vehicleType, decimal balance)
		{
			// The type of constructor is shown in the tests and the constructor should have a validation, 
			// which also is clear from the tests.
			//  регулярное выражение для Id @"^[A-Z]{2}-\d{4}-[A-Z]{2}$"
			if (Regex.IsMatch(id, @"^[A-Z]{2}-\d{4}-[A-Z]{2}$"))
			{
				this.Id = id;
			}
			else
			{
				throw new ArgumentException("Wrong ID format. Should be XX-YYYY-XX (where X is any uppercase letter of the English alphabet and Y is any number)");
			}
			if (balance >= 0)
			{
				this.Balance = balance;
			}
			else
			{
				throw new ArgumentException("Wrong balance. Balance must not be negative.");
			}
			this.VehicleType = vehicleType;
		}
		public static string GenerateRandomRegistrationPlateNumber()
		{
			//Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
			char[] letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
			char[] numbers = "1234567890".ToCharArray();
			Random random = new Random();
			string result = "";
			for (int i = 0; i < 10; i++)
			{
				if (i < 2 || i > 7)
					result += letters[random.Next(0, letters.Length)];
				else if (i == 2 || i == 7) result += "-";
				else result += numbers[random.Next(0, numbers.Length)];
			}
			return result;
		}
		public override string ToString()
		{
			return $"{Id} {VehicleType} {Balance}";
		}
	}
}